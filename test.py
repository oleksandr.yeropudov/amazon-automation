from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time
from datetime import datetime
from config import AMAZON_USERNAME, AMAZON_PASSWORD
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import json

class Client:
    def __init__(self):
        # used for JSPhantom
        dcap = dict(DesiredCapabilities.PHANTOMJS)
        dcap["phantomjs.page.settings.userAgent"] = (
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/53 "
            "(KHTML, like Gecko) Chrome/15.0.87"
        )
        self.client = driver= webdriver.Firefox(executable_path="./geckodriver")
        self.log = {}
    def start(self):
        self.client.get('https://www.amazon.com')
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            logInLink = WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.ID, 'nav-link-accountList')))
            self.go_to_login()
        except TimeoutException:
            print('start')
            self.client.quit()
        finally:
            self.log[key] = 'go login form'
            print('go login form...')

    def go_to_login(self):
        logInLink = self.client.find_element_by_id('nav-link-accountList')
        logInLink.click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.NAME,'email')))
            self.fill_login_form()
        except TimeoutException:
            print('go_to_login')
            self.client.quit()
        finally:
            self.log[key] = 'go fill login form'
            print('go fill login form...')

    def fill_login_form(self):
        login_filed = self.client.find_element_by_name('email').send_keys(AMAZON_USERNAME)
        password_field = self.client.find_element_by_name('password').send_keys(AMAZON_PASSWORD)
        self.client.find_element_by_id('signInSubmit').click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR,'.nav-line-3')))
            self.go_to_gift()
        except TimeoutException:
            print('fill_login_form')
            self.client.quit()
        finally:
            self.log[key] = 'go to gift page'
            print('go to gift page...')

    def go_to_gift(self):
        self.client.get('https://www.amazon.com/gift-cards/')
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            link = WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.XPATH,'//img[@alt="eGift"]/parent::a')))
            self.go_to_product(link)
        except TimeoutException:
            print('go_to_gift')
            self.client.quit()
        finally:
            self.log[key] = 'go to product'
            print('go to product...')

    def go_to_product(self, link):
        link.click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.ID,'gc-order-form')))
            self.fill_product_form()
        except TimeoutException:
            print('go_to_product')
            self.client.quit()
        finally:
            self.log[key] = 'go fill product form'
            print('go fill product form...')

    def fill_product_form(self):
        self.client.find_element_by_id('gc-customization-type-button-Designs').click()
        time.sleep(0.5)
        self.client.find_element_by_id('gc-mini-design-thumb-2').click()
        time.sleep(0.5)
        self.client.find_element_by_id('gc-order-form-custom-amount').send_keys('50')
        time.sleep(0.5)
        self.client.find_element_by_id('gc-order-form-recipients').send_keys('oleksandr.yeropudov@gmail.com')
        time.sleep(0.5)
        self.client.find_element_by_id('gc-order-form-senderName').send_keys('Me')
        time.sleep(0.5)
        self.client.find_element_by_id('gc-order-form-quantity').clear()
        self.client.find_element_by_id('gc-order-form-quantity').send_keys('2')
        time.sleep(0.5)
        self.client.find_element_by_id('gc-buy-box-bn').click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.CSS_SELECTOR,'.new-payment-methods-header')))
            self.process_order()
        except TimeoutException:
            print('fill_product_form')
            self.client.quit()
        finally:
            self.log[key] = 'go CC fill'
            print('go CC fill...')

    def process_order(self):
        # CC Holder Name
        self.client.find_element_by_id('ccName').send_keys('YOUR CC name')
        # CC num
        self.client.find_element_by_id('addCreditCardNumber').send_keys('YOUR CCC number')
        time.sleep(0.5)
        self.client.execute_script("document.getElementById('ccMonth').style.display = 'block';")
        selectMonth = Select(self.client.find_element_by_id("ccMonth"))
        selectMonth.select_by_visible_text("05")
        time.sleep(0.5)
        self.client.execute_script("document.getElementById('ccYear').style.display = 'block';")
        selectYear = Select(self.client.find_element_by_id("ccYear"))
        selectYear.select_by_value("2018")
        time.sleep(1)
        self.client.find_element_by_id('ccAddCard').click()
        time.sleep(2)
        # continue order
        self.client.find_element_by_id('continue-top').click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            WebDriverWait(self.client, 20).until(EC.presence_of_element_located((By.ID,'enterAddressFullName')))
            self.fill_address()
        except TimeoutException:
            print('process_order')
            self.client.quit()
        finally:
            self.log[key] = 'go Billing Addr fill'
            print('go Billing Addr fill...')

    def fill_address(self):
        self.client.find_element_by_id('enterAddressFullName').send_keys('CLAYTON ATCHISON')
        self.client.find_element_by_id('enterAddressAddressLine1').send_keys('1 822 TESON RD')
        self.client.find_element_by_id('enterAddressCity').send_keys('HAZELWOOD')
        self.client.find_element_by_id('enterAddressStateOrRegion').send_keys('MO')
        self.client.find_element_by_id('enterAddressPostalCode').send_keys('63042')
        # SELECT COUNTRY United States
        selectCountry = Select(self.client.find_element_by_id("enterAddressCountryCode"))
        selectCountry.select_by_visible_text('United States')
        self.client.find_element_by_id('enterAddressPhoneNumber').send_keys('+1 314-506-8400')
        # SUBMIT
        self.client.find_element_by_name('continue').click()
        key = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            continue_link = WebDriverWait(self.client, 20).until(
                EC.presence_of_element_located((By.XPATH,'//a[@class="a-declarative a-button-text checkout-continue-link"]'))
            )
            self.confirm_check_out(continue_link)
        except TimeoutException:
            print('fill_address')
            self.client.quit()
        finally:
            self.log[key] = 'go confirm checkout'
            print('go confirm checkout...')
    def confirm_check_out(self, link):
        link.click()
        with open('log.json','w+') as fp:
            json.dump(self.log, fp=fp)

if __name__ == '__main__':
    client = Client()
    client.start()






