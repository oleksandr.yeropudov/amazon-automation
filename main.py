from reader import Reader
from sys import argv

try:
    script, file_path = argv
    reader = Reader(file_path)
    source_data = reader.read_source_file()
    print(source_data)
except ValueError:
    print("Please provide path to source file")

