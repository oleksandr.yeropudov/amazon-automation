import os
import requests
import json

from config import AMAZON_PASSWORD, AMAZON_USERNAME
from grab import Grab
class AmazonClient:
    def __init__(self):
        self.client = Grab()
        self.client.setup(follow_location=True)
        self.client.setup(timeout=60)
        self.client.setup(connect_timeout=60)
        self.sign_in_link = ""
        self.gift_link = ""
    def login(self):
        self.client.go('https://www.amazon.com/')
        link_to_log_in = self.client.doc.select('//a[@data-nav-role="signin"]/@href').text()
        if link_to_log_in is None:
            pass
        else:
            self.client.go(link_to_log_in)
            self.client.doc.choose_form(name="signIn")
            self.client.doc.set_input('password', AMAZON_PASSWORD)
            self.client.doc.set_input("email", AMAZON_USERNAME)
            self.client.doc.submit()
            account_block = self.client.doc.select('//a[@id="nav-link-accountList"]').text()
            if account_block is None:
                print("Login failure")
                return False
            else:
                print("OK", account_block)
                return True

    def go_to_gifts(self):
        links_to_gift = self.client.doc.select('//a[text() = "Gift Cards & Registry"]/@href').text()
        if links_to_gift is None:
            return False

        self.client.go(links_to_gift)
        links_to_gift = self.client.doc.select('//img[@alt="eGift Cards &gt; See all"]/parent::a/@href').text()
        self.gift_link = links_to_gift
        self.client.go(links_to_gift)


    def choose_gift_params(self):
        params = {"searchImageUrl":"https://images-na.ssl-images-amazon.com/images/I/41HIBCiSxzL.jpg","brandType":"Amazon","designImageUrl":"https://images-na.ssl-images-amazon.com/images/G/01/gc/designs/livepreview/a_generic_white_10_us_noto_email_v2016_us-main._CB277146614_.png","deliveryMechanism":"Email","designTemplateVersion":"v2016","deliveryType":"Email","occasions":{},"designId":"a_generic_white_10_us_noto_email_v2016_us","asin":"B004LLIKVU","swatchImageUrl":"https://images-na.ssl-images-amazon.com/images/I/31cvqMhdG2L.jpg","title":"Amazon 'a'","customizationType":"Designs"}
        link = "https://www.amazon.com/gc/d/miniPicker?parentAsin=BT00DC6QU4"+\
               "&minAmount=0&deliveryMechanism=NOT_SPECIFIED&customizationTypes=STANDARD"+\
               "&deliveryType=&occasions=&maxAmount=0&asin=B004LLIKVU"
        #params = json.loads(params)
        print(params)

if __name__ == "__main__":
    client = AmazonClient()
    if client.login():
        client.go_to_gifts()
        client.choose_gift_params()
        # Запомнить ссылку что бы после обнов параметров опять открывать ее
        #Форма для заказа form id gc-order-form
        #Выбираем дизигн, тип гифта и прочие данные, все есть в атрибуте data-gc-select-design. Выбираем по  data-action gc-select-design => по ссылке открываем => https://www.amazon.com/gc/d/miniPicker?parentAsin=BT00DC6QU4&minAmount=0&deliveryMechanism=NOT_SPECIFIED&customizationTypes=STANDARD&deliveryType=&occasions=&maxAmount=0&asin=B004LLIKVU
        #пишем номинал карты в id gc-order-form-custom-amount $500

