from openpyxl import load_workbook


class Reader:

    __slots__ = ['file_path', 'collection']

    def __init__(self, filepath):
        self.file_path = filepath

    def read_source_file(self):
        wb = load_workbook(self.file_path)
        anotherSheet = wb.active
        print("Total cards num", anotherSheet.max_row)
        return [anotherSheet.cell(row=i, column=4).value for i in range(2, anotherSheet.max_row)]
