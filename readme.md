#SET UP#

1) clone repo

2) go to project dir

3) run python -m venv env # https://docs.python.org/3/library/venv.html

4) run . env/bin/activate

5) run pip install -r requirements.txt

6) put to project dir phantomjs and geckodriver executables

7) copy config.py.dist to config.py and fill credentials for amazon
#RUN#

8) for testing put in test.py your data (like email, cc holder, cc number, cvc and etc.
9) run python test.py


